package com.cognitivei.plexmetadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Joiner;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import static com.google.common.base.Preconditions.*;

public class MbChannelCache {
    Client mHttpClient;
    static final String API_KEY = "3771c636a28141ac806e511a7d6c299e";

    Map<String, String> mChannelDatabase = new HashMap<>(5000);

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Channels {

        @JsonIgnoreProperties(ignoreUnknown = true)
        private static class Channel {
            public String title;
            public String id;

            @Override
            public String toString() {
                return "[" + id + ", " + title + "]";
            }
        }

        public List<Channel> channels;

        @Override
        public String toString() {
            return Joiner.on(", ").join(channels);
        }
    }


    public MbChannelCache() {
        ClientConfig cc = new ClientConfig().register(new JacksonFeature());
        mHttpClient = ClientBuilder.newClient(cc);

        Optional<PrintWriter> writer = Optional.empty();

        try {
            writer = Optional.of(new PrintWriter("channeldatabase.txt", "UTF-8"));
        }
        catch(Exception e) {
            System.err.println("Unable to write channeldatabase");
        }


        int offset = 0;
        final int WINDOW_SIZE = 100;

        int count = 0;
        final int MAX_CHANNELS = 10000;
        while(count < MAX_CHANNELS) {
            Channels result = mHttpClient.target("https://atlas.metabroadcast.com")
                    .path("/4/channels.json")
                    .queryParam("offset", offset)
                    .queryParam("limit", WINDOW_SIZE)
                    .queryParam("annotations", "channel")
                    .queryParam("key", API_KEY)
                    .request(MediaType.APPLICATION_JSON_TYPE)
                    .get(Channels.class);


            // ingest
            for(Channels.Channel c: result.channels) {
                String key = c.title;
                if(mChannelDatabase.containsKey(key))
                    System.err.println("duplication title:" + key);

                mChannelDatabase.put(key, c.id);
                if(writer.isPresent()) {
                    writer.get().println(c.id + "," + key);
                }

                count++;
            }

            // are we done?
            if((0 == result.channels.size()) || (WINDOW_SIZE != result.channels.size())) {
                break;
            }

            offset += WINDOW_SIZE;
        }

        if(writer.isPresent()) {
            writer.get().close();
        }
    }

    public String get(String channelTitle) {
        return checkNotNull(mChannelDatabase.get(channelTitle));
    }


}
