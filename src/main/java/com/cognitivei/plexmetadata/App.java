package com.cognitivei.plexmetadata;


import org.apache.commons.io.FilenameUtils;

public class App {


    public static void main(String args[]) {

        MbScheduleLookup lookup = new MbScheduleLookup();

        for(String f: args) {

            try {
                ReduxFilenameParser parser = new ReduxFilenameParser(FilenameUtils.getBaseName(f));
                MbScheduleLookup.Episode episode = lookup.getBroadcast(parser.getChannel(), parser.getBroadcastDate());

                String path = FilenameUtils.getFullPath(f);
                String extension = FilenameUtils.getExtension(f);
                String newBasename = episode.toString();

                String newFilename = newBasename + FilenameUtils.EXTENSION_SEPARATOR_STR + extension;
                System.out.println("ln \"" + f + "\" \"" + FilenameUtils.concat(path, newFilename) + "\"");
            } catch(Exception e) {
                System.err.println("Error processing: " + f);
                System.err.println(e.getMessage());
            }

        }


    }
}
