package com.cognitivei.plexmetadata;

import com.google.common.collect.ImmutableMap;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class ReduxFilenameParser {

    ZonedDateTime mTransmissionDate;
    String mChannel;

    final static Map<String, String> CHANNEL_LOOKUP = InitialiseChannelMapper();

    public ReduxFilenameParser(String filename) {
        // all Redux timestamps are UTC
        String broadcastDateRaw = filename.substring(0, 15);
        LocalDateTime transmission = LocalDateTime.parse(broadcastDateRaw, DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss"));
        mTransmissionDate = ZonedDateTime.of(transmission, ZoneId.of("UTC"));

        // out of bounds exception is okay to be thrown here
        String[] parts = filename.split("_");
        mChannel = CHANNEL_LOOKUP.get(parts[2]);
    }

    public ZonedDateTime getBroadcastDate() {
        return mTransmissionDate;
    }

    public String getChannel() {
        return mChannel;
    }

    private static Map<String, String> InitialiseChannelMapper() {
        return new ImmutableMap.Builder<String, String>()
                .put("bbcone", "BBC One")
                .put("bbctwo", "BBC Two")
                .put("bbcthree", "BBC Three")
                .put("bbcfour", "BBC Four")
                .put("cbbc", "CBBC")
                .put("cbeebies", "CBeebies")
                .put("bbcnews24", "News 24")
                .put("bbchd", "BBC HD")
                .put("bbcparl", "BBC Parliament")
                .put("bbconehd", "BBC One HD")
                .put("bbctwohd", "BBC Two HD")
                .put("bbcthreehd", "BBC Three HD")
                .put("bbcfourhd", "BBC Four HD")
                .put("cbbchd", "CBBC HD")
                //.put("cbeebieshd", "CBeebies HD") // we don't have CBeebies HD in MB sources
                .put("cbeebieshd", "CBeebies")
                .put("bbcnewshd", "BBC News HD")
                .put("302-dsat-sd", "302 (DSAT, SD)")
                .put("54381-dsat-sd", "54381 (DSAT, SD)")
                .put("303-dtt-hd", "303 (DTT, HD)")
                .put("302-dtt-sd", "302 (DTT, SD)")
                .put("303-dsat-hd", "303 (DSAT, HD)")
                .put("bbcrb3", "BBC RB 3")
                .put("bbcrb1", "BBC RB 1")
                .put("bbcrb2", "BBC RB 2")
                .put("bbcolympic1", "BBC Olympics 1")
                .put("bbcolympic2", "BBC Olympics 2")
                .put("bbcolympic3", "BBC Olympics 3")
                .put("bbcolympic4", "BBC Olympics 4")
                .put("bbcolympic5", "BBC Olympics 5")
                .put("bbcolympic6", "BBC Olympics 6")
                .put("bbcolympic7", "BBC Olympics 7")
                .put("bbcolympic8", "BBC Olympics 8")
                .put("bbcolympic9", "BBC Olympics 9")
                .put("bbcolympic10", "BBC Olympics 10")
                .put("bbcolympic11", "BBC Olympics 11")
                .put("bbcolympic12", "BBC Olympics 12")
                .put("bbcolympic13", "BBC Olympics 13")
                .put("bbcolympic14", "BBC Olympics 14")
                .put("bbcolympic15", "BBC Olympics 15")
                .put("bbcolympic16", "BBC Olympics 16")
                .put("bbcolympic17", "BBC Olympics 17")
                .put("bbcolympic18", "BBC Olympics 18")
                .put("bbcolympic19", "BBC Olympics 19")
                .put("bbcolympic20", "BBC Olympics 20")
                .put("bbcolympic21", "BBC Olympics 21")
                .put("bbcolympic22", "BBC Olympics 22")
                .put("bbcolympic23", "BBC Olympics 23")
                .put("bbcolympic24", "BBC Olympics 24")
                .put("bbcolympichd1", "BBC Olympics HD 1")
                .put("bbcolympichd2", "BBC Olympics HD 2")
                .put("bbcolympichd3", "BBC Olympics HD 3")
                .put("bbcolympichd4", "BBC Olympics HD 4")
                .put("bbcolympichd5", "BBC Olympics HD 5")
                .put("bbcolympichd6", "BBC Olympics HD 6")
                .put("bbcolympichd7", "BBC Olympics HD 7")
                .put("bbcolympichd8", "BBC Olympics HD 8")
                .put("bbcolympichd9", "BBC Olympics HD 9")
                .put("bbcolympichd10", "BBC Olympics HD 10")
                .put("bbcolympichd11", "BBC Olympics HD 11")
                .put("bbcolympichd12", "BBC Olympics HD 12")
                .put("bbcolympichd13", "BBC Olympics HD 13")
                .put("bbcolympichd14", "BBC Olympics HD 14")
                .put("bbcolympichd15", "BBC Olympics HD 15")
                .put("bbcolympichd16", "BBC Olympics HD 16")
                .put("bbcolympichd17", "BBC Olympics HD 17")
                .put("bbcolympichd18", "BBC Olympics HD 18")
                .put("bbcolympichd19", "BBC Olympics HD 19")
                .put("bbcolympichd20", "BBC Olympics HD 20")
                .put("bbcolympichd21", "BBC Olympics HD 21")
                .put("bbcolympichd22", "BBC Olympics HD 22")
                .put("bbcolympichd23", "BBC Olympics HD 23")
                .put("bbcolympichd24", "BBC Olympics HD 24")
                .put("itv1", "ITV1")
                .put("itv2", "ITV2")
                .put("itv3", "ITV3")
                .put("itv4", "ITV4")
                .put("channel4", "Channel 4")
                .put("e4", "E4")
                .put("more4", "More4")
                .put("s4c", "S4C")
                .put("dave", "Dave")
                .put("five", "Five")
                .put("c4para1", "C4 Paralympics 1")
                .put("c4para2", "C4 Paralympics 2")
                .put("c4para3", "C4 Paralympics 3")
                .put("c4para1hd", "C4 Paralympics 1 HD")
                .put("c4para2hd", "C4 Paralympics 2 HD")
                .put("c4para3hd", "C4 Paralympics 3 HD")
                .put("301", "Interactive 301")
                .put("302", "Interactive 302")
                .put("bbcrb22101", "BBC Red Button 22101")
                .put("bbcrb22102", "BBC Red Button 22102")
                .put("bbcrb22103", "BBC Red Button 22103")
                .put("bbcrb22104", "BBC Red Button 22104")
                .put("bbcrb22105", "BBC Red Button 22105")
                .put("yadvashem", "Yad Vashem")
                .put("bbc1ci", "BBC 1 Channel Islands")
                .put("bbc1east", "BBC 1 East")
                .put("bbc1emids", "BBC 1 East Midlands")
                .put("bbc1london", "BBC 1 London")
                .put("bbc1nec", "BBC 1 North East and Cumbria")
                .put("bbc1ni", "BBC 1 Northern Ireland")
                .put("bbc1nihd", "BBC 1 Northern Ireland HD")
                .put("bbc1nwest", "BBC 1 North West")
                .put("bbc1oxford", "BBC 1 Oxford")
                .put("bbc1scothd", "BBC 1 Scotland HD")
                .put("bbc1scotland", "BBC 1 Scotland")
                .put("bbc1seast", "BBC 1 South East")
                .put("bbc1south", "BBC 1 South")
                .put("bbc1swest", "BBC 1 South West")
                .put("bbc1wales", "BBC 1 Wales")
                .put("bbc1waleshd", "BBC 1 Wales HD")
                .put("bbc1west", "BBC 1 West")
                .put("bbc1wmids", "BBC 1 West Midlands")
                .put("bbc1yorks", "BBC 1 Yorkshire")
                .put("bbc1yrkslin", "BBC 1 Yorks and Lincs")
                .put("bbc2england", "BBC 2 England")
                .put("bbc2ni", "BBC 2 NI")
                .put("bbc2scotland", "BBC 2 Scotland")
                .put("bbc2wales", "BBC 2 Wales")
                .put("bbcwn", "BBC World News")
                .put("bbcalba", "BBC Alba")
                .put("bbcr1", "Radio 1")
                .put("bbc1x", "Radio 1 Xtra")
                .put("bbcr2", "Radio 2")
                .put("bbcr3", "Radio 3")
                .put("bbcr4", "Radio 4")
                .put("bbcr5l", "Five Live")
                .put("r5lsx", "Five Live Sports")
                .put("bbc6m", "Six Music")
                .put("bbc7", "BBC 7")
                .put("bbcan", "Asian Network")
                .put("bbcws", "World Service")
                .put("bbcrscotland", "BBC Radio Scotland")
                .put("bbcrngael", "BBC Radio Nan Gaedheal")
                .put("bbcrwales", "BBC Radio Wales")
                .put("bbcrcymru", "BBC Radio Cymru")
                .put("bbcrulster", "BBC Radio Ulster")
                .put("bbcrlondon", "BBC Radio London")
                .build();
    }


}
