package com.cognitivei.plexmetadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;

public class MbScheduleLookup {
    static final String API_KEY = "3771c636a28141ac806e511a7d6c299e";

    MbChannelCache mChannelCache = new MbChannelCache();
    ClientConfig mCc = new ClientConfig().register(new JacksonFeature());

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class ScheduleResponse {

        @JsonIgnoreProperties(ignoreUnknown = true)
        static class Schedule {

            @JsonIgnoreProperties(ignoreUnknown = true)
            static class Entry {

                @JsonIgnoreProperties(ignoreUnknown = true)
                static class Item {
                    public String id;
                }

                @JsonIgnoreProperties(ignoreUnknown = true)
                static class Broadcast {
                    public String transmission_time;
                    public String transmission_end_time;
                }

                public Item item;
                public Broadcast broadcast;
            }

            public List<Entry> entries;

        }
        public Schedule schedule;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Episode {

        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Brand {
            public String title;
        }

        public String type;
        public String id;
        public String description;
        public int episode_number;
        public int series_number;
        public String title;


        public Brand container;

        @Override
        public String toString() {
            int s = Math.max(1, series_number);
            return String.format("%s - s%02de%02d", container.title, s, episode_number);
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    static class ContentResponse {
        public Episode episode;
    }


    public Episode getBroadcast(String channelTitle, ZonedDateTime transmissionTime) {
        Client httpClient = getHttpClient();
        String channelId = mChannelCache.get(channelTitle);

        ScheduleResponse scheduleResult = this.getSchedule(httpClient, channelId, transmissionTime, ScheduleResponse.class);

        if((1 == scheduleResult.schedule.entries.size())
                && (transmissionTime.isEqual(ZonedDateTime.parse(scheduleResult.schedule.entries.get(0).broadcast.transmission_time)))) {

            String needle = scheduleResult.schedule.entries.get(0).item.id;
            ContentResponse response = this.getEpisode(httpClient, needle, ContentResponse.class);
            return response.episode;
        }
        else
            throw new NoSuchElementException();
    }

    Client getHttpClient() {
        return ClientBuilder.newClient(mCc);
    }

    <R> R getEpisode(Client httpClient, String needle, Class<R> klass) {
        // generics used here so can flip between String and ContentResponse types "easily"
        // klass is required because of generics type erasure means we can't R.class in the .get
        return  httpClient.target("https://atlas.metabroadcast.com")
                .path("/4/content/" + needle + ".json")
                .queryParam("key", API_KEY)
                .queryParam("annotations", "description,brand_summary")
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(klass);
    }

    <R> R getSchedule(Client httpClient, String mbChannel, ZonedDateTime transmissionTime, Class<R> klass) {
        // generics used here so can flip between String and ScheduleResponse types "easily"
        // klass is required because of generics type erasure means we can't R.class in the .get
        String tt = transmissionTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);

        return httpClient.target("https://atlas.metabroadcast.com")
                .path("/4/schedules/" + mbChannel + ".json")
                .queryParam("from", tt)
                .queryParam("count", 1)
                .queryParam("source", "pressassociation.com")
                .queryParam("annotations", "")
                .queryParam("key", API_KEY)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get(klass);
    }



}
