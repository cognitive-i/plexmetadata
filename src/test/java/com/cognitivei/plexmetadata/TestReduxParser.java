package com.cognitivei.plexmetadata;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;

public class TestReduxParser {

    @Test
    public void goodDate() {
        // we could parameterise this test, but we'll be testing Java LocalDateTime mostly
        String stick = "20121009_143000_cbeebies_raa_raa_the_noisy_lion-lo";
        ZonedDateTime stickDateTime = ZonedDateTime.of( LocalDateTime.of(2012, 10, 9, 14, 30, 0, 0), ZoneId.of("UTC"));

        ReduxFilenameParser sut = new ReduxFilenameParser(stick);
        assertEquals(stickDateTime, sut.getBroadcastDate());
    }

    @Test(expected=DateTimeParseException.class)
    public void badDateTooManyDays() {
        // we could parameterise this test, but we'll be testing Java LocalDateTime mostly
        String stick = "20121032_143000_cbeebies_raa_raa_the_noisy_lion-lo";
        new ReduxFilenameParser(stick);
    }

    @Test(expected=DateTimeParseException.class)
    public void badDateFunnyTime() {
        // we could parameterise this test, but we'll be testing Java LocalDateTime mostly
        String stick = "20121031_253000_cbeebies_raa_raa_the_noisy_lion-lo";
        new ReduxFilenameParser(stick);
    }

    @Test(expected=StringIndexOutOfBoundsException.class)
    public void shortFilename() {
        String stick = "";
        new ReduxFilenameParser(stick);
    }

    @Test(expected=ArrayIndexOutOfBoundsException.class)
    public void missingChannel() {
        String stick = "20121031_143000";
        new ReduxFilenameParser(stick);
    }

    @Test
    public void goodChannels() {
        // we could parameterise this test but we'll mostly be testing a map
        String stick;
        ReduxFilenameParser sut;

        stick = "20121031_143000_cbeebies_raa_raa_the_noisy_lion-lo";
        sut = new ReduxFilenameParser(stick);
        assertEquals("CBeebies", sut.getChannel());

        stick = "20121031_143000_cbeebieshd_raa_raa_the_noisy_lion-lo";
        sut = new ReduxFilenameParser(stick);
        assertEquals("CBeebies", sut.getChannel());
    }



}

