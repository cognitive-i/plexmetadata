package com.cognitivei.plexmetadata;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class TestKnownGoodEpisodes {

    String mNeedleChannel;
    ZonedDateTime mNeedleBroadcastTime;
    String mExpectedBrand;
    int mExpectedSeries;
    int mExpectedEpisode;

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                // broadcast, channel, brand, series, episode
                {"20150127_173000", "CBeebies", "Boj", 1, 41},
                {"20150223_092500", "CBeebies", "Twirlywoos", 1, 1},
                {"20150403_052000", "CBeebies", "Pingu", 0, 25},     // no series for Pingu
                {"20150212_072500", "CBeebies", "Hey Duggee", 1, 24},

                // recent raaa raa
                {"20150401_143000", "CBeebies", "Raa Raa the Noisy Lion", 0, 7},

                /*
                // problematic episodes
                // see Test2012Data
                {"20120816_143000", "CBeebies", "Raa Raa the Noisy Lion", 0, 0}, // close
                {"20120817_143000", "CBeebies", "Raa Raa the Noisy Lion", 0, 0}, // close
                {"20120820_143000", "CBeebies", "Raa Raa the Noisy Lion", 0, 0}, // not found (index out of bounds)
                */

                /*
                // seems CBeebies metadata is broken over christmas on XMas
                // but CBeebiesHD is okay!  Unfortunately MB doesn't give me CBeebiesHD
                // worse Atlas gives me an ID and content fails for it
                {"20141224_172500", "CBeebies", "Sarah & Duck", 0, 0} // Sarah & Duck Seacow XMas special
                */

                // {"", "CBeebies", "", 0, 0},
        });
    }

    public TestKnownGoodEpisodes(String reduxDatetime, String channel, String brand, int series, int episode) {

        mNeedleBroadcastTime = ZonedDateTime.of(
                LocalDateTime.parse(reduxDatetime, DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss")),
                ZoneId.of("UTC"));

        mNeedleChannel = channel;

        mExpectedBrand = brand;
        mExpectedSeries = series;
        mExpectedEpisode = episode;

    }

    @Test
    public void ValidateBroadcast() {
        MbScheduleLookup sut = new MbScheduleLookup();
        MbScheduleLookup.Episode actual = sut.getBroadcast(mNeedleChannel, mNeedleBroadcastTime);

        assertEquals(mExpectedBrand, mExpectedBrand, actual.container.title);
        assertEquals(mExpectedBrand, mExpectedSeries, actual.series_number);
        assertEquals(mExpectedBrand, mExpectedEpisode, actual.episode_number);
    }

}
