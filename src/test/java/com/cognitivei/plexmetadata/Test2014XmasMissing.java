package com.cognitivei.plexmetadata;

import org.junit.Test;

import javax.ws.rs.client.Client;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Test2014XmasMissing {

    @Test
    public void WhiteboxScheduleTest() {
        MbScheduleLookup sut = new MbScheduleLookup();

        Client httpClient = sut.getHttpClient();
        String channelId = sut.mChannelCache.get("CBeebies");

        ZonedDateTime broadcast = ZonedDateTime.of(
                LocalDateTime.parse("20141224_172500", DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss")),
                ZoneId.of("UTC"));

        MbScheduleLookup.ScheduleResponse scheduleResult = sut.getSchedule(httpClient, channelId, broadcast, MbScheduleLookup.ScheduleResponse.class);


        // do we have found an broadcast that matches our transmission start time
        assertEquals(1, scheduleResult.schedule.entries.size());

        MbScheduleLookup.ScheduleResponse.Schedule.Entry needle = scheduleResult.schedule.entries.get(0);

        assertTrue(broadcast.isEqual(ZonedDateTime.parse(needle.broadcast.transmission_time)));

        // pull out id and get content
        String id = needle.item.id;
        MbScheduleLookup.ContentResponse response = sut.getEpisode(httpClient, id, MbScheduleLookup.ContentResponse.class);
        assertEquals("Sarah & Duck", response.episode.container.title);
    }

}
