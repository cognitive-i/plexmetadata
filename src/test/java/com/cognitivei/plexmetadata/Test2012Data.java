package com.cognitivei.plexmetadata;

import org.junit.Test;

import javax.ws.rs.client.Client;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

public class Test2012Data {
    @Test
    public void WhiteboxOldEpisodeTest() {

        ZonedDateTime broadcast = ZonedDateTime.of(
                LocalDateTime.parse("20120816_143000", DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss")),
                ZoneId.of("UTC"));

        MbScheduleLookup sut = new MbScheduleLookup();
        MbScheduleLookup.Episode episode = sut.getBroadcast("CBeebies", broadcast);

        assertEquals("Raa Raa the Noisy Lion", episode.container.title);
    }

    @Test
    public void WhiteboxScheduleTest() {
        MbScheduleLookup sut = new MbScheduleLookup();

        Client httpClient = sut.getHttpClient();
        String channelId = sut.mChannelCache.get("CBeebies");

        ZonedDateTime broadcast = ZonedDateTime.of(
                LocalDateTime.parse("20120816_143000", DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss")),
                ZoneId.of("UTC"));

        MbScheduleLookup.ScheduleResponse scheduleResult = sut.getSchedule(httpClient, channelId, broadcast, MbScheduleLookup.ScheduleResponse.class);

        assertEquals(1, scheduleResult.schedule.entries.size());
        assertTrue("transmission date mismatch", broadcast.isEqual(ZonedDateTime.parse(scheduleResult.schedule.entries.get(0).broadcast.transmission_time)));
    }
}
